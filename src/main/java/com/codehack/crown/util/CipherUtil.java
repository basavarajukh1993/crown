package com.codehack.crown.util;

import org.springframework.stereotype.Component;

@Component
public class CipherUtil {
    private final int CIPHER_KEY = 4;

    public String decipher(String encodedString) {
        StringBuilder decryptMessage = new StringBuilder();
        for (int index = 0; index < encodedString.length(); index++) {
            char alphabet = encodedString.charAt(index);
            if (Character.isAlphabetic(alphabet)) decryptMessage.append(decryptAlpha(alphabet));
            else decryptMessage.append(alphabet);
        }
        return decryptMessage.toString();
    }

    private char decryptAlpha(char alphabet) {
        if (Character.isLowerCase(alphabet)) {
            alphabet = (char) (alphabet - CIPHER_KEY);
            if (!Character.isAlphabetic(alphabet)) {
                alphabet = (char) (alphabet - 'a' + 'z' + 1);
            }
            return alphabet;
        } else if (Character.isUpperCase(alphabet)) {
            alphabet = (char) (alphabet - CIPHER_KEY);
            if (!Character.isAlphabetic(alphabet)) {
                alphabet = (char) (alphabet - 'A' + 'Z' + 1);
            }
            return alphabet;
        }
        return alphabet;
    }
}

