package com.codehack.crown.model;

import java.util.Objects;

public class BattleMessage {
    private KINGDOM_NAME name;
    private String message;

    public BattleMessage(KINGDOM_NAME name, String message) {
        this.name = name;
        this.message = message;
    }

    public KINGDOM_NAME getName() {
        return name;
    }

    public void setName(KINGDOM_NAME name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BattleMessage that = (BattleMessage) o;
        return name == that.name &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, message);
    }
}
