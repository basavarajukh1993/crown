package com.codehack.crown.model;

public enum  EMBLEM {
    GORILLA,
    PANDA,
    OCTOPUS,
    MAMMOTH,
    OWL,
    DRAGON
}
