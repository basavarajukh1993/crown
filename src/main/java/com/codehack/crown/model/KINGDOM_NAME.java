package com.codehack.crown.model;

public enum KINGDOM_NAME {
    SPACE,
    LAND,
    WATER,
    ICE,
    AIR,
    FIRE
}
