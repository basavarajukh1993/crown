package com.codehack.crown.model;

import java.util.Objects;

public class Kingdom {
    private KINGDOM_NAME name;
    private EMBLEM emblem;

    public Kingdom(KINGDOM_NAME name, EMBLEM emblem) {
        this.name = name;
        this.emblem = emblem;
    }

    public KINGDOM_NAME getName() {
        return name;
    }

    public void setName(KINGDOM_NAME name) {
        this.name = name;
    }

    public EMBLEM getEmblem() {
        return emblem;
    }

    public void setEmblem(EMBLEM emblem) {
        this.emblem = emblem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kingdom kingdom = (Kingdom) o;
        return name == kingdom.name &&
                emblem == kingdom.emblem;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, emblem);
    }
}
