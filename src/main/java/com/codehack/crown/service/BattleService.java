package com.codehack.crown.service;

import com.codehack.crown.model.BattleMessage;
import com.codehack.crown.model.KINGDOM_NAME;
import com.codehack.crown.service.handler.KingdomHandler;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class BattleService {
    Set<KINGDOM_NAME> successKingdomSet = new HashSet<>();
    //TODO: Yet to add unit tests
    public String process(Set<BattleMessage> battleMessages) {
        battleMessages.forEach((battleMessage -> {
            KingdomHandler handler = KingdomFactory.getKingdomHandler(battleMessage.getName());
            if (handler.validateMessage(battleMessage.getMessage())) {
                successKingdomSet.add(battleMessage.getName());
            }
        }));
        if (successKingdomSet.size() >= 3) {
            return Arrays.toString(successKingdomSet.toArray());
        }
        return "None";
    }
}
