package com.codehack.crown.service;

import com.codehack.crown.model.KINGDOM_NAME;
import com.codehack.crown.service.handler.*;

public class KingdomFactory {
    public static KingdomHandler getKingdomHandler(KINGDOM_NAME name) {
        switch (name) {
            case AIR:
                return new AirKingdomHandler();
            case ICE:
                return new IceKingdomHandler();
            case LAND:
                return new LandKingdomHandler();
            case FIRE:
                return new FireKingdomHandler();
            case WATER:
                return new WaterKingdomHandler();
            case SPACE:
                return new SpaceKingdomHandler();
            default:
                throw new IllegalArgumentException("Unknown Kingdom");
        }
    }
}
