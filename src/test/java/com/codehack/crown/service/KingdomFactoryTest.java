package com.codehack.crown.service;

import com.codehack.crown.model.KINGDOM_NAME;
import com.codehack.crown.service.handler.*;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;

class KingdomFactoryTest {
    @Test
    void shouldValidateFactory() {
        assertThat(KingdomFactory.getKingdomHandler(KINGDOM_NAME.AIR), instanceOf(AirKingdomHandler.class));
        assertThat(KingdomFactory.getKingdomHandler(KINGDOM_NAME.FIRE), instanceOf(FireKingdomHandler.class));
        assertThat(KingdomFactory.getKingdomHandler(KINGDOM_NAME.ICE), instanceOf(IceKingdomHandler.class));
        assertThat(KingdomFactory.getKingdomHandler(KINGDOM_NAME.LAND), instanceOf(LandKingdomHandler.class));
        assertThat(KingdomFactory.getKingdomHandler(KINGDOM_NAME.SPACE), instanceOf(SpaceKingdomHandler.class));
        assertThat(KingdomFactory.getKingdomHandler(KINGDOM_NAME.WATER), instanceOf(WaterKingdomHandler.class));
    }
}