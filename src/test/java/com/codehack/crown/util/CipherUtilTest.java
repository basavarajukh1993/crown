package com.codehack.crown.util;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
class CipherUtilTest {
    @Autowired
    CipherUtil util;

    @Test
    void shouldDecipherGivenText() {
        assertThat(util.decipher("efgh"), is("abcd"));
        assertThat(util.decipher("abcd"), is("wxyz"));

        assertThat(util.decipher("EFGH"), is("ABCD"));
        assertThat(util.decipher("ABCD"), is("WXYZ"));

        assertThat(util.decipher("EFGH123"), is("ABCD123"));
        assertThat(util.decipher("ABCD..,"), is("WXYZ..,"));
    }
}